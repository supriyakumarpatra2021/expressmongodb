const express = require('express');

const app = express();
const setting = require("./setting/setting");
const movieRouter = require('./Routes/movie');
const mongoose = require('mongoose');



mongoose.connect(setting.CONN_STR).then((conn)=>{
  // console.log(conn);
  console.log('db connection sucessfull');
}).catch((err)=>{
  console.log('some error has ocurred',err);
});

app.use(express.json())
app.use('/api/v1/movies',movieRouter);

module.exports = app;

