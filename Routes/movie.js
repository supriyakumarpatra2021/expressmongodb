const express = require('express');
const router = express.Router();
const controller = require('./../controller/movie');

router.route('/movie-stats').get(controller.moviesStats)
router.route('/movie-genre/:genName').get(controller.movieByGenre)
router.route('/highest-rated').get(controller.getHighestRated, controller.getAllMovie)
router.route('/').get(controller.getAllMovie).post(controller.createMovie);
router.route('/:id').get(controller.getMovie).patch(controller.updateMovie).delete(controller.deleteMovie);


module.exports = router