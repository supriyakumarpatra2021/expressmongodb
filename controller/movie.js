const Movie = require('./../models/movie');

exports.validateBody =(req,res,next)=>{
    if(!req.body.name || !req.body.releaseYear){
        return res.status(400).json({
            status:'fail',
            message:'Not a valid movie data'
        })
    }
    next()
}

exports.getHighestRated = async (req,res,next)=>{
    req.query.limit = '5';
    req.query.sort = '-ratings';
    next();
}

exports.getAllMovie = async (req,res)=>{
    try {
        console.log(req.query.sort);
        // const excludeFields =['sort','page','limit','fields']
       let queryMovies  =  Movie.find();
       if(req.query.sort){
        const sortBy = req.query.sort.split(',').join(' ');
        console.log(sortBy)
        queryMovies =  queryMovies.sort(sortBy)
       }else(
        queryMovies =  queryMovies.sort('-createdAt')
       )

       if(req.query.fields){
        const fields = req.query.fields.split(',').join(' ');
        queryMovies.select(fields)
       }

    //PEGINATION
       const page = +req.query.page || 1;
       const limit = +req.query.limit || 10;
       //page 1:1-10; page 2: 11-20; page 3: 21-30;
       const skip = (page-1)*limit;
       queryMovies = queryMovies.skip(skip).limit(limit);

       if(req.query.page){
        const moviesCount = await Movie.countDocuments();
        if(skip>=moviesCount){
            throw new Error('This page is not found!')
        }
       }

      const allMovies = await queryMovies
       res.status(200).json({
        status:'success',
        length:allMovies.length,
            data:{
                movies:allMovies
            }
       })
    } catch (err) {
        res.status(404).json({
            status:'fail',
            err:err.message
        })
    }
    
    
}

exports.getMovie = async (req,res)=>{
    try {
       const movies  = await Movie.findById(req.params.id);
       res.status(200).json({
        status:'success',
            data:{
                movies:movies
            }
       })
    } catch (err) {
        res.status(404).json({
            status:'fail',
            err:err.message
        })
    }
    
    
}

exports.createMovie=async (req,res)=>{
    try {
        console.log(req.body)
        const movie = await Movie.create(req.body);
        res.status(201).json({
            status:'success',
            data:{
                movie
            }
        })
    } catch (error) {
        res.status(500).json({
            status:'fail',
            err:error.message
        })
        
    }

    
}

exports.updateMovie = async (req,res)=>{
    try {
        const updatedMovie = await Movie.findByIdAndUpdate(req.params.id,req.body,{
            new:true,runValidators:true
        })
        res.status(200).json({
            status:'success',
            data:{
                movie:updatedMovie
            }
        })

    } catch (error) {
        res.status(500).json({
            status:'fail',
            err:error.message
        })
    }
}

exports.deleteMovie = async (req,res)=>{
    try {
        await Movie.findByIdAndDelete(req.params.id)
        res.status(204).json({
            status:'success',
            data:null
        })
    } catch (error) {
        res.status(500).json({
            status:'fail',
            err:error.message
        })
        
    }
}

exports.moviesStats = async(req,res)=>{
    try {
        // console.log('............')
        const stats = await Movie.aggregate([
            {
                $match:{
                    ratings:{
                        $gte:4.5
                    }
                }
            },
            {
                $group:{
                    _id:'$releaseYear',
                    avgRating:{
                        $avg:'$ratings'
                    },
                    avgPrice:{
                        $avg:'$price'
                    },
                    maxPrice:{
                        $max:'$price'
                    },
                    minPrice:{
                        $min:'$price'
                    },
                    totalPrice:{
                        $sum:'$price'
                    },
                    movieCount:{
                        $sum:1
                    }
                }
            },
            {
                $sort:{
                    minPrice:1
                }
            },
            {
                $match:{
                    maxPrice:{
                        $gte:60
                    }
                }
            },
        ]);
        console.log(stats)
        res.status(200).json({
            status:'success',
            count:stats.length,
            data:{stats}
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status:'fail',
            err:err.message
        })
    }
}


exports.movieByGenre = async(req,res)=>{
    try {
        const genreName = req.params.genName
        const genre = await Movie.aggregate([
            {
                $unwind:'$genres'
            },
            {
                $group:{
                    _id:'$genres',
                    movieCount:{$sum:1},
                    movies:{$push:'$name'}
                }
            },
            {
                $addFields:{
                    genre:'$_id'
                }
            },
            {
                $project:{_id:0}
            },
            {
                $sort:{movieCount:-1}
            },
            {
                $limit:6
            },
            {
                $match:{genre:genreName}
            }
        ]);
        res.status(200).json({
            status:'success',
            count:genre.length,
            data:{genre}
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status:'fail',
            err:err.message
        })
    }
}