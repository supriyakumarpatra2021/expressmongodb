const mongoose = require('mongoose');
const movieSchema = new mongoose.Schema({
    name:{
      type:String,
      required:[true,'Name is required field'],
      unique:true
    },
    description:{
      type:String,
      required:[true,'Description is required field'],
      trim:true
    },
    duration:{
      type:Number,
      required:[true,'Duration is required field']
    },
    ratings:{
      type:Number,
      default:1.0
    },
    totalRating:Number,
    releaseYear:{
      type:Number,
      required:[true,'Release Year is required field']

    },
    releaseDate:{
      type:Date
    },
    createdAt:{
      type:Date,
      default: Date.now()
    },
    genres:{
      type:[String],
      required:[true,'Genres is a required field']
    },
    directors:{
      type:[String],
      required:[true,'directors is a required field']
    },
    coverImage:{
      type:String,
      required:[true,'cover image is a required field']
    },
    price:{
      type:Number,
      required:[true,'price is a required field']
    }
  
  });
  const Movie = mongoose.model('Movie',movieSchema);

  module.exports = Movie