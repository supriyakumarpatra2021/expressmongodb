const mongoose=require('mongoose');
const fs = require('fs');
const setting = require('./../setting/setting');
const Movie = require('./../models/movie');


mongoose.connect(setting.CONN_STR).then((conn)=>{
    // console.log(conn);
    console.log('db connection sucessfull');
  }).catch((err)=>{
    console.log('some error has ocurred',err);
  });

  const movies = JSON.parse(fs.readFileSync('./data/movies.json','utf-8'));

  const deleteMovies  = async()=>{
    try {
        await Movie.deleteMany();
        console.log('Data successfully deleted');
    } catch (error) {
        console.log(error.message)
    }
    process.exit()
  }

  const createMovies  = async()=>{
    try {
        await Movie.create(movies);
        console.log('Data successfully Imported');
    } catch (error) {
        console.log(error.message)
    }
    process.exit()
  }

  console.log(process.argv)
  if(process.argv[2]=='--import'){
    createMovies()
  }

  if(process.argv[2]=='--delete'){
    deleteMovies()
  }

//   deleteMovies()
//   createMovies()